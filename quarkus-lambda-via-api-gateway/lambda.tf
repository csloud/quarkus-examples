resource "aws_lambda_function" "QuarkusLambdaCamel" {
  filename         = var.path_to_maven_generated_quarkus_assembly
  function_name    = "QuarkusLambdaCamel"
  role             = aws_iam_role.iam_for_quarkus_lambda_camel.arn
  handler          = "io.chrisloud.ExampleResource"
  source_code_hash = filebase64sha256(var.path_to_maven_generated_quarkus_assembly)
  runtime          = "provided"
}

