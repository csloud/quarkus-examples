resource "aws_api_gateway_rest_api" "MyDemoAPI" {
  name        = "MyDemoAPI"
  description = "This is my API for demonstration purposes"
}

resource "aws_lambda_permission" "lambda_permission" {
  statement_id  = "AllowMyDemoAPIInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "QuarkusLambdaCamel"
  principal     = "apigateway.amazonaws.com"

  # The /*/*/* part allows invocation from any stage, method and resource path
  # within API Gateway REST API.
  source_arn = "${aws_api_gateway_rest_api.MyDemoAPI.execution_arn}/*/*/*"
}

resource "aws_api_gateway_resource" "MyDemoAPIResource" {
  rest_api_id = "${aws_api_gateway_rest_api.MyDemoAPI.id}"
  parent_id   = "${aws_api_gateway_rest_api.MyDemoAPI.root_resource_id}"
  path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "MyDemoAPIMethod" {
  rest_api_id   = "${aws_api_gateway_rest_api.MyDemoAPI.id}"
  resource_id   = "${aws_api_gateway_resource.MyDemoAPIResource.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "MyDemoAPIIntegration" {
  rest_api_id = "${aws_api_gateway_rest_api.MyDemoAPI.id}"
  resource_id = "${aws_api_gateway_resource.MyDemoAPIResource.id}"
  http_method = "${aws_api_gateway_method.MyDemoAPIMethod.http_method}"
  integration_http_method = "POST"
  type        = "AWS_PROXY"
  uri         = "${aws_lambda_function.QuarkusLambdaCamel.invoke_arn}"
}

resource "aws_api_gateway_deployment" "dev" {
  depends_on  = ["aws_api_gateway_integration.MyDemoAPIIntegration"]
  rest_api_id = "${aws_api_gateway_rest_api.MyDemoAPI.id}"
}

resource "aws_api_gateway_stage" "dev" {
  stage_name    = "dev"
  rest_api_id   = "${aws_api_gateway_rest_api.MyDemoAPI.id}"
  deployment_id = "${aws_api_gateway_deployment.dev.id}"
}
