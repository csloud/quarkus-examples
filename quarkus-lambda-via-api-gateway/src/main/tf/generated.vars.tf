variable "path_to_maven_generated_quarkus_assembly" {
  type = string
  default = "${project.build.directory}/function-java/${project.build.finalName}.zip"
}
