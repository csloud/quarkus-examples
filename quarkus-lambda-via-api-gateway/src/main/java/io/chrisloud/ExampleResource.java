package io.chrisloud;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import io.chrisloud.model.Order;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExampleResource implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExampleResource.class);

    ObjectWriter writer = new ObjectMapper().writerFor(ServiceStatus.class);

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent request, final Context context) {
        final Optional<Order> order = getOrderFromRequestBody(request.getBody());
 
        ServiceStatus result = new ServiceStatus();

        String greeting = "Received order #" + order.map(Order::getId).orElse("NA");
        result.setGreeting(greeting);
        result.setContext(context.toString());

        LOGGER.info(greeting);
        //context.getLogger().log("Test lambda logger");

        try {
            return new APIGatewayProxyResponseEvent().withBody(writer.writeValueAsString(result)).withStatusCode(200);
        } catch (JsonProcessingException e) {
            LOGGER.error("Error processing", e);
            return new APIGatewayProxyResponseEvent().withBody(e.getMessage()).withStatusCode(500);
        }
    }

    protected Optional<Order> getOrderFromRequestBody(String requestBody) {
        try {
            return Optional.of(new ObjectMapper().readValue(
                StringUtils.defaultString(requestBody), Order.class));
        } catch (IOException e) {
            LOGGER.error("Could not deserialize request body to Order.", e);
        }
        return Optional.empty();
    }

}
