package io.chrisloud;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.chrisloud.model.Order;

import io.quarkus.amazon.lambda.test.LambdaClient;
import io.quarkus.amazon.lambda.test.LambdaException;
import io.quarkus.test.junit.QuarkusTest;

import java.util.Optional;

@QuarkusTest
public class ExampleResourceTest {

    private ExampleResource exampleResource = new ExampleResource();

    @Test
    public void getOrderFromRequestBody_with_valid_body() throws Exception {
        String body = "{\"id\":\"1234\"}";
        Optional<Order> order = exampleResource.getOrderFromRequestBody(body);
        Assertions.assertEquals("1234", order.map(Order::getId).orElse("EMPTY"),
            "The id from the JSON should match the Order object id property");
    }

    @Test
    public void getOrderFromRequestBody_with_invalid_body() throws Exception {
        String body = "{\"name\":\"1234\"}"; // Order does not have a name property
        Optional<Order> order = exampleResource.getOrderFromRequestBody(body);
        Assertions.assertEquals("EMPTY", order.map(Order::getId).orElse("EMPTY"),
            "Id should equal EMPTY if the request body cannot be deserialized to an Order");
    }

    @Test
    public void getOrderFromRequestBody_with_null_body() throws Exception {
        String body = null;
        Optional<Order> order = exampleResource.getOrderFromRequestBody(body);
        Assertions.assertEquals("EMPTY", order.map(Order::getId).orElse("EMPTY"),
            "Id should equal EMPTY if the request body is null");
    }

}
