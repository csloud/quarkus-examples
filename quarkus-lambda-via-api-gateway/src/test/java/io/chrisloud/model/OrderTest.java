package io.chrisloud.model;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
 
/**
 *
 * @author topherloud
 */
public class OrderTest {

    @Test
    public void testDeserialization() throws Exception {
        String json = "{\"id\":\"1234\"}";
        Order order = new ObjectMapper().readValue(json, Order.class);
        Assertions.assertEquals("1234", order.getId(),
            "The id from the JSON should match the Order object id property");
    }    
}
